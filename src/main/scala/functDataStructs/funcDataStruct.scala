//package functDataStructs

/*data types are defined by "traits". Sealed means its means the* implementation of the trait must be within this same
file. A trait is an abstract interface that may (optionally) contain the implementation of some method(s)
 Note that using a general "A" means the Type is polymorphic*/
sealed trait List[+A] //"+" means the type parameter A is a "covariant", means it allows subtypes (ex: animals AND dogs)
//Two implementation of the trait are listed here (using the keyword "case":
//The first one is the empty list
case object Nil extends List[Nothing]
//The second type (construct) rep non empty list, which are defined by a "head element followed by another list (potentially empty)
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List { //Companion object. are tied to new traits. Has same name as data type (ex: List). We place convenience function here
  def sum(ints:List[Int]):Int = ints match {
    case Nil => 0 //sum of empty list is 0
    case Cons(x, xs) => x + sum(xs) //sum of list starting with x is x plus the sum of the rest of the list
  }

  /*pattern matching example. Starts definition with a "target" (here "ds") and the keyword "match" followed by a
  sequence of cases, each consisting of a pattern on the left of => and a result top the right
  If the pattern matches the a case, the result of that case becomes the result of the entire match expression.
  If multiple pattern are matches, the first one listed will be picked by Scala*/
  def product(ds:List[Double]): Double = ds match{ //pattern matching
    case Nil => 1.0 //product of empty list is 1/0
    case Cons(0.0, _) => 0.0 //product of any list starting with 0.0 will be 0.0
    case Cons(x,xs) => x * product(xs) //anything else is equal the the current element times the product of the rest of the list
  }

  //return list excluding first element
  //O(len(ds))
  def tail[A](ds:List[A]):List[A] = ds match {
    case Nil => sys.error("tail of empty list")
    case Cons(_,xs) => xs

  }

  //replaces first element of list
  //O(len(ds))
  def setHead[A](ds:List[A], y:A):List[A] = ds match {
    case Nil => sys.error("Cant replace head of empty list")
    case Cons(_,xs) => Cons(y,xs)
  }

  //drop first n element of the List
  //O(n), not O len(l)
  def drop[A](l:List[A], n:Int):List[A] = {
    if (n <= 0) l
    else l match {
      case Nil => Nil
      case Cons(_,x) => drop(x, n-1)
    }
  }
  //original implementation
  /*def drop[A](l:List[A], n:Int):List[A] = {
    @annotation.tailrec
    def go(m:List[A], o:Int, acc:Int): List[A] = {
      if (m == Nil) Nil
      else if (o == acc) tail(m)
      else go(tail(m), o, acc+1)
    }
    go(l, n, acc=1)
  }*/

  //drop elements from top down as long as they match a predicate
  def dropWhile[A](l:List[A], f: A => Boolean): List[A]= l match{
    case Nil => Nil
    case Cons(x,xs) if f(x) => dropWhile(xs, f)
    case _ => l
  }
  //curried version
  /* dropWhile(l:List[A]) returns a "temp" function that takes a function f: A => Boolean as an arg
  the advantage here is that we can call the dropWhile function using anon function without specifying
  the type. Ex: Non curried: dropWhile(List(1,2,3,4),(x:Int) => x < 3)
  Curried:  dropWhile(List(1,2,3,4))(x => x < 4)
  The key here is that type information flows from left to right. The call to the first function
  dropWhile[A](l:List[A]) already determined that A was an Int. The temp function then created would look
  something like temp(f: Int => Boolean). There is then no need to specify the type in the anon function
  since it has been determined by the type of the elements of the list in the first function call.
   */
  def dropWhileCurried[A](l:List[A]) (f: A => Boolean): List[A]= l match{
    case Nil => Nil
    case Cons(x,xs) if f(x) => dropWhileCurried(xs)(f)
    case _ => l
  }

  //append 2 lists
  //Note that this appends values only until the first list is exhausted and then will simply point to a2
  //So, time and memory only depend on the size of a1
  def append[A](a1: List[A], a2: List[A]):List[A] = a1 match {
    case Nil => a2
    case Cons(h,t) => Cons(h, append(t, a2))
  }

  def appendElem[A](ll: List[A], elem:A):List[A] = {
    append(ll, List(elem))
    //case Nil => elem
    //case Cons(h, t) => Cons(h, appendElem(t, elem))
  }

  //returns list except the last element
  //much less efficient this time
  def init[A](l:List[A]):List[A] = l match {
    case Nil => Nil
    case Cons(_, Nil) => Nil
    case Cons(h,t) => Cons(h, init(t))
  }

  /*Variadic functions
  * Accepts 0 or more arguments of type "A"
  * This is the function that allows syntax like "List(1,2,3,4,5)" which is calling apply in the background*/
  def apply[A](as: A*): List[A] = { //Variadic function syntax. The star allows us to pass a "Seq" from the Scala API
    //because of "apply", we can pass a Seq of element explicitly (Seq is part of the Scala API
    //as will be bound to Seq[A] which as functions head (first elem) and tail (all except 1st elem)
    //The special _* allows us to pass a Aeq to a variadic function
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail:_*))
  }

  //Note, this implementation is not tail-recusive, hence not stack-safe (potential overflows on big lists
  //z is the value to return instead of an empty list
  def foldRight[A,B](as:List[A], z:B)(f: (A,B) => B):B = as match {
    case Nil => z
    case Cons(x,xs) => f(x, foldRight(xs, z)(f))
  }
  def sum2(ns:List[Int]):Int = foldRight(ns, 0)((x,y)=>x+y)
  def product2(ns:List[Double]):Double = foldRight(ns, 1.0)(_ * _)

  //Length of a list using foldRight
  //Here, we add 1 for every element encountered until Nil, which will simply add 0
  def length[A](ns:List[A]):Int = foldRight(ns, 0)((_,y) => 1+y)

  //tail recursive version of foldright
  //here, z is the value to assign as the starting point of our accumulator
  //once the Nil case is reached, all computation is done and it returns directly
  @annotation.tailrec
  def foldLeft[A,B](as:List[A], z:B)(f: (B,A) => B):B = as match {
    case Nil => z
    case Cons(x,xs) => foldLeft(xs, f(z,x))(f)
  }

  //longer too verbose 1st version with useless go sub func
  def foldLeft2[A,B](as:List[A], z:B)(f: (B,A) => B):B = {
    @annotation.tailrec
    def go(as:List[A], acc:B): B = as match {
      case Nil => acc
      case Cons(x,xs) => go(xs,f(acc, x))
    }
    go(as, z)

  }

  def length2[A](as:List[A]):Int = foldLeft(as, 0)((acc,_)=>acc+1)

  def append2[A](as1:List[A], as2:List[A]): List[A] = {
    foldRight(as2, as1)(Cons(_,_))
  }

  //Slower
  def catListListL[A](ll:List[List[A]]):List[A] = foldLeft(ll, List[A]())(append)
  //Faster
  def catListListR[A](ll:List[List[A]]):List[A] = foldRight(ll, List[A]())(append)
  /*Why? Because of the waY THE APPEND FUNCTION operates. When appending, The function goes
  * through every element of the list to the left and when it reaches the end, adds a pointer to the
  * beginning of the list on the right. This means the time to compute depends on the list to the left.
  * If we use fold left, operations will start growing as we progress toward the solution since we grow
  * the final list from left to right
  * [[1,2],[3,4],[5,6]] --> [[1,2,3,4],[5,6]] --> [1,2,3,4,5,6]
  * We should take advantage of the fact that the append function behavior doesn't depend on the list on
  * the right side by growing it from the right , which is exactly what foldright does
  * [[1,2],[3,4],[5,6]] --> [[1,2], [3,4,5,6]] --> [1,2,3,4,5,6]
  * That way, way, the upper bound per "merge operation" is the length of the bigger list in the List of
  * Lists.
   */
  def addOneToAll2(l:List[Int]):List[Int] = {
    foldRight(l, List[Int]())((h,t) => Cons(h+1, t))
  }

  def addOneToAll(l:List[Int]):List[Int] = {
    @annotation.tailrec
    def go(m: List[Int], acc: List[Int]): List[Int] = m match {
      case Nil => acc
      case Cons(x,xs) => {
        val nx = List[Int](x+1)
        go(xs, append(acc,nx))
      }
    }

    go(l, acc = List[Int]())
  }
  def doubleToStr(l:List[Double]):List[String] = {
    foldRight(l, List[String]())((h,t) => Cons(h.toString, t))
  }

  def reverse[A](l: List[A]): List[A] = foldLeft(l, List[A]())((acc,h) => Cons(h,acc))

  //make a stack safe version of
  def foldRightViaFoldLeft[A,B](l: List[A], z: B)(f: (A,B) => B): B =
    foldLeft(reverse(l), z)((b,a) => f(a,b))

  def map[A,B](as: List[A])(f:A => B):List[B] = foldRightViaFoldLeft(as, List[B]())((h,t) => Cons(f(h), t))

  def filter[A](as:List[A])(f:A => Boolean): List[A]= {
    foldRightViaFoldLeft(as, List[A]())((h,t) => if (f(h)) Cons(h,t) else t)
  }

  def flatmap[A,B](as: List[A])(f:A => List[B]):List[B] = {
    foldRightViaFoldLeft(as, List[B]())((h,t) => append(f(h), t))
  }

  def filter2[A](as:List[A])(f:A => Boolean): List[A]= {
    flatmap(as)(a => if (f(a)) List(a) else Nil)
  }

  //Write function that accepts 2 lists and adds corresponding elements
  def addupLists(l1:List[Int], l2:List[Int]):List[Int] = (l1,l2) match {
    case (Nil,Nil) => Nil
    case (Cons(h1,_),Nil) => Cons(h1, Nil)
    case (Nil,Cons(h2,_)) => Cons(h2, Nil)
    case (Cons(h1,t1), Cons(h2,t2)) => Cons(h1+h2,addupLists(t1,t2))
  }

  //Takes two lists, run function on element at same indices, returns single list
  def zipWith[A,B,C](l1:List[A], l2:List[B])(f:(A,B) => C):List[C] = (l1,l2) match {
    case (Nil,Nil) => Nil
    case (Cons(h1,t1), Cons(h2,t2)) => Cons(f(h1,h2),zipWith(t1,t2)(f))
  }

  @annotation.tailrec
  def hasSubsequence[A](sup:List[A], sub: List[A]):Boolean = (sup,sub) match{
    case (_,Nil) => true
    case (Nil,_) => false
    case (Cons(h1,t1), Cons(h2,t2)) => if (h1 == h2) hasSubsequence(t1,t2) else hasSubsequence(t1, sub)
  }

}






/*Pattern matching example

scala> List(1,2,3) match { case _ => 42 }
res0: Int = 42

scala> List(1,2,3) match { case Cons(h,_) => h }
<console>:15: warning: match may not be exhaustive.
It would fail on the following input: Nil

res1: Int = 1


List(1,2,3) match { case Cons(_,t) => t }
<console>:15: warning: match may not be exhaustive.
It would fail on the following input: Nil

res2: List[Int] = Cons(2,Cons(3,Nil))


scala> List(1,2,3) match { case Nil => 42 }
<console>:14: warning: match may not be exhaustive.
It would fail on the following input: Cons(_, _)

scala.MatchError: Cons(1,Cons(2,Cons(3,Nil))) (of class Cons)

*/