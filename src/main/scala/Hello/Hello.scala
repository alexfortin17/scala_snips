package Hello

/*
Some definitions:

Higher Order functions: Functions that take function(s) as input(s)
Signature: Part of the function definition that is before the "=" sign
Definition: Part of the function definition that is after the "=" sign
 */

object Hello {

  //FORMATTING FUNCTIONS
  private def formatAbs(x: Int) : String = {
    val msg = "The absolute value of %d is %d"
    msg.format(x, abs(x))
  }

  private def formatFibo(x: Int) : String = {
    val msg = "The %dth fibonacci number is %d"
    msg.format(x, fibonacci(x))
  }

  private def formatFacto(x: Int) : String = {
    val msg = "The factorial value of %d is %d"
    msg.format(x, factorial(x))
  }

  //Higher order function that takes a function as an input
  def formatResult(name: String, n: Int, f: Int => Int): String = {
    val msg = "The %s of %d is %d"
    msg.format(name, n, f(n))
  }


  // USING RECURSIONS AS A REPLACEMENT FOR LOOPS
  /*In order to use recursion efficiently, we must make sure that our recursive calls are always in tail position
  * This means that a recursive calls must be only to the function itself without any work to be added
  * For example. the following factorial function could create stack overflows since it is not in tail position (because
  * of the "n*" in front of the recursive call
  * def facto(n:Int):Int={
  * if (n<=0) 1
  * else n*facto(n-1)
  * }
  * We typically use a helper function (called go or loop by convention) as well as accumulator to make sure all
  *  our recursive calls are in tail position
  * The @annotation.tailrec decorator signals to the compiler that we intend a tail recursive call on the following
  * function and will throw an error at compile time if we fail to do so
  * */

  def abs (n: Int) : Int =
    if (n < 0) -n
    else n

  def factorial (n:Int) : Int = {
    @annotation.tailrec //would fail to compile if recursion not in tail position (No optimization possible)
    def go(n: Int, acc: Int): Int = {
      if (n <= 0) acc
      else go(n - 1, n * acc)
    }
    go(n, 1)
  }

  //@annotation.tailrec //This will fail to compile if indicated since recursion is NOT in tail position
  def shittyFacto(n:Int):Int={
    if (n <= 0) 1
    else n*shittyFacto(n-1)
  }



  def fibonacci(n:Int):Int={
    @annotation.tailrec
    def go(n:Int, mem1:Int, mem2:Int, acc:Int):Int={
      if (acc == n) mem1.+(mem2)
      else go(n, mem2, mem1.+(mem2), acc.+(1))
    }
    if (n <= 0){
      0
    }
    else if (n == 1){
      0
    }
    else if (n == 2){
      1
    }
    else{
      go(n, mem1 = 0, mem2 = 1, acc = 3)
    }
  }

  //MONOMORPHIC vs POLYMORPHIC FUNCTIONS
  //In this example, the two function returns the index of the array of the first element that matches x

  //Monomorphic version
  //Note that the structure of that function would be the same if we were looking for an certain int instead of a str
  def findFirst(ss:Array[String], key: String): Int = {
    @annotation.tailrec
    def loop(n:Int): Int = {
      if (n >= ss.length) -1
      else if (ss(n) == key) n
      else loop(n+1)
    }
    loop(0)
  }

  /*Polymorphic version, where the same algo can be applied for arrays of different types
  we replaced the key by a function that evaluates an element A of the array "as". That function could be any function
  that returns a boolean*/
  def findFirst[A](as:Array[A], p: A => Boolean): Int = {
    @annotation.tailrec
    def loop(n:Int): Int = {
      if (n >= as.length) -1
      else if (p(as(n))) n
      else loop(n+1)
    }
    loop(0)
  }

  /*This function takes an array of element of a certain type and a function that compares 2 element of that type
  * It then runs the comparison function of each pair of element in order to verify if the array is sorted*/
  def isSorted[A](as: Array[A], ordered: (A,A) => Boolean): Boolean = {
    @annotation.tailrec
    def loop(n:Int): Boolean = {
      if (n >= as.length) true
      else if (ordered(as(n-1), as(n))) loop(n+1)
      else false
    }
    if (as.length <= 1){
      true
    }
    else loop(1)
  }

  def orderedInt(x:Int, y:Int): Boolean = x <= y

  /*ANONYMOUS FUNCTION
  * Example:
  * findFirst(Array(1,9,10), (x:Int) => x == 9)
  * The anon function here is (x:Int) => x == 9
  * It is the equivalent of a function with signature: anon(x:Int):Boolean
  *  and definition if (x == 9) true; else false;
  * */


  /*partial application
  Here, based on the signature , there's only one way to define the definition*/
  def partial1[A,B,C](a:A, f: (A,B) => C) : B => C = {
    b:B => f(a,b)
  }

  def partEx(strAr:Array[String], intAr:Array[Int]): Boolean = {
    if (strAr.length == intAr.length) true
    else false
  }

  /*Converting a function f of two args in a function of 1 arg where f was partially applied
  * Notice the use of anonymous function in the definition
  * Once we have the signature, in order to deduce the definition, we look at the available ways to express certain
  * values (replace C by f(a,b) for example) and fill in the blank using anonymous values*/
  def curry[A,B,C](f: (A,B) => C): A => B => C = {
    a:A => b:B => f(a,b)
  }

  def uncurry[A,B,C](f: A => B => C): (A,B) => C = {
    (a:A, b:B) => f(a)(b)
  }

  def toCurry(a:Int, b:Double) : String = {
    "%d %.2f".format(a, b)
  }


  /*COMPOSITION*/
  def compose[A,B,C](f:B=>C, g:A=>B): A => C = {
    a:A => f(g(a))
  }
  //There is a compose method for all Function1s
  //You can use f compose g or g andThen f to represent f(g(x))

  def compose1(a: Int): String = {
    a.toString
  }

  def compose2(b:String): Double = {
    b.length.toDouble
  }

  /*MAIN
  * In Scala, the main function is the outer shell around the functional core
  * main must always have the same signature: def main(args: Array[String]): Unit
  * */
  def main(args: Array[String]): Unit = {

    println("\n\nTesting recursive functions\n")
    println(formatAbs(-42))
    println(formatFacto(5))
    //println(shittyFacto(5))
    println(formatFibo(7))

    println("\n\nTesting Anonymous functions\n")
    /*Here, (x:Int) => x == 9 in an anonymous function (declared "on the fly)*/
    findFirst(Array(1,9,10), (x:Int) => x == 9)

    println("\n\nTesting polymorphic functions\n")
    println(formatResult("Fibonacci number", 7, fibonacci))
    println(formatResult("Factorial", 7, factorial))
    println(formatResult(name = "absolute", -7, abs))
    println("Sorted? %b".format(isSorted(Array(1,2,3), orderedInt)))
    println("Sorted? %b".format(isSorted(Array(1,2,3,3), orderedInt)))
    println("Sorted? %b".format(isSorted(Array(1,2,3,2), orderedInt)))

    println("\n\nTesting partial functions\n")
    //same length, should be true
    println(partial1(Array("a", "b", "c"), partEx)(Array(1,2,3)))
    //diff length should be false
    println(partial1(Array("a", "b", "c"), partEx)(Array(1,2,3,4)))

    println("\n\nTesting curry functions\n")
    val curriedFunc = curry(toCurry)
    println(curriedFunc(2)(2.2))

    println("\n\nTesting composition\n")
    //here, we apply compose1 first and then compose2
    val composedFunc = compose(compose2, compose1)
    /*compose can be called on function, not method. The underscore changes the method to a function
    Note that compose is used as f compose g for f(g(x))
     */
    val composedFunc2 = compose2 _ compose compose1
    println(compose2(compose1(545)))
    println(composedFunc(545))
    println(composedFunc2(545))
  }
}
